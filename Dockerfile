# ---------------Tomcat------------------

FROM tomcat:8.5.82-jdk8-temurin-jammy

WORKDIR /usr/local/tomcat/webapps

COPY ROOT.war ./ROOT.war

CMD ["catalina.sh", "run"]

EXPOSE 8080
